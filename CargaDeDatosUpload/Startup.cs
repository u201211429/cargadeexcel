﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CargaDeDatosUpload.Startup))]
namespace CargaDeDatosUpload
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
